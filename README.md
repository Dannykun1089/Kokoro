# Kokoro

## Setup
1. __Create the Bot and Invite it to your Server__

In order to start your own instance of this bot you must first create a discord bot application on discord's [developer portal](https://discord.com/developers/)

Press "Create Application" and give it a unique name, press enter to create it. 
After your application has been created, go to the tab labeled "Bot" and press the button labeled "Add Bot" and confirm, this will create the bot account.
In the same tab, press the button labeled "Reset Token", this will generate a fresh bot token which the program uses to access the bot account, copy this and store it somewhere secure for later usage.
Lastly, scroll down to two options labeled "server members intent" and "message content intent" under the "Privileged Gateway Intents" section, and enable them both, this allows the bot to access other users and the messages they send.

The bot account has been created, now you must invite it to your server

In your application page, click on the "OAuth2" side tab and select "URL Generator" from the dropdown.
This will bring up a page with a list of checkboxes, check the one labeled "bot".
This will bring up another set of checkboxes bellow it where you can select the permissions the link will request for the bot.
You can check them manually if you want, but for now just copy the link at the bottom and replace "&permissions=0" with "&permissions=517580651840".

Enter this link into your browser and discord will prompt you to invite it to a server you are admin/owner of


2. __Set up PostgreSQL__

This bot uses PostgreSQL to manage server settings such as DJ perms, embed color, etc. [TODO]

3. __Set up LavaLink__

LavaLink is a peice of software this bot uses for playing audio, it handles downloading from a variety of sites as well as the other complicated stuff

LavaLink requires a java install to run since it is a java program, ideally install version 11, since thats what they say to use.
You should be able to find the latest LavaLink release [here](https://github.com/freyacodes/Lavalink/releases) under the name Lavalink.jar.
Place the Lavalink.jar file somewhere appropriate like the project folder, and create a file in the same directory as it called "application.yml"

Open "application.yml" in a text editor and paste the following data into it
```
server: # REST and WS server
  port: 1234
  address: 127.0.0.1
spring:
  main:
    banner-mode: log
lavalink:
  server:
    password: "sample123"
    sources:
      youtube: true
      bandcamp: true
      soundcloud: true
      twitch: true
      vimeo: true
      mixer: true
      http: true
      local: false
    bufferDurationMs: 400
    youtubePlaylistLoadLimit: 6 # Number of pages at 100 each
    youtubeSearchEnabled: true
    soundcloudSearchEnabled: true
    gc-warnings: true

metrics:
  prometheus:
    enabled: false
    endpoint: /metrics

sentry:
  dsn: ""
#  tags:
#    some_key: some_value
#    another_key: another_value

logging:
  file:
    max-history: 30
    max-size: 1GB
  path: ./logs/

  level:
    root: INFO
    lavalink: INFO
```

And change the password value to something secure
open up a command prompt and type in the command "```java -jar Lavalink.jar```" and it should start running.

4. __Set up Environment__
TODO

## Credits

|Name|Repository|Website| 
|:--:|:--:|:--:|
|DSharp Plus|[[Github]](https://github.com/DSharpPlus/DSharpPlus)|[[Website]](https://dsharpplus.github.io/)|
|Magick.NET|[[Github]](https://github.com/dlemstra/Magick.NET)|N/A|
|BooruSharp|[[Github]](https://github.com/Xwilarg/BooruSharp)|N/A|
|NpgSQL|[[Github]](https://github.com/npgsql/npgsql)|N/A|
|Serilog|[[Github]](https://github.com/serilog/serilog)|[[Website]](https://serilog.net)