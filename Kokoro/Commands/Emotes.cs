﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Kokoro.Extensions;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using static Kokoro.Extensions.DSharpPlus;

namespace Kokoro.Commands
{
    public class Emotes : BaseCommandModule
    {
        private string[] HugImages { get; }
        private string[] PatImages { get; }
        private string[] KissImages { get; }
        private string[] SmackImages { get; }
        private string[] StareImages { get; }
        private string[] QuestionImages { get; }
        private string[] LaughImages { get; }
        private string[] BoobiesImages { get; }

        private Random Rng { get; set; }


        public Emotes()
        {
            var emotesJson = JObject.Parse(File.ReadAllText("./Static/Data/Emotes.json"));

            HugImages = emotesJson.GetValue("hug").ToObject<string[]>();
            PatImages = emotesJson.GetValue("pat").ToObject<string[]>();
            KissImages = emotesJson.GetValue("kiss").ToObject<string[]>();
            SmackImages = emotesJson.GetValue("smack").ToObject<string[]>();
            StareImages = emotesJson.GetValue("stare").ToObject<string[]>();
            QuestionImages = emotesJson.GetValue("question").ToObject<string[]>();
            LaughImages = emotesJson.GetValue("laugh").ToObject<string[]>();
            BoobiesImages = emotesJson.GetValue("boobies").ToObject<string[]>();

            Rng = new Random((int)DateTimeOffset.Now.ToUnixTimeSeconds());
        }


        [Command("hug"), Aliases("cuddle")]
        public async Task Hug(CommandContext ctx, [RemainingText] string userIdentifier)
        {
            // If there is no user identifier, send a unique message
            if (userIdentifier is null)
            {
                await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                       .WithColor(new DiscordColor("FF0080"))
                                                       .WithDescription($"here you go {ctx.User.Mention}!\n^_^")
                                                       .WithImageUrl(Rng.NextElement(HugImages)));
                return;
            }

            // Get the user by their identifier, alert the user if none found
            var targetMember = await GetMemberByIdentifier(ctx, userIdentifier);
            if (targetMember is null)
            {
                await ctx.Channel.SendMessageAsync("Unable to find the user");
                return;
            }

            // if the user is trying to hug themselves, we must laugh at them
            if (ctx.User.Id == targetMember.Id)
            {
                await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                    .WithColor(new DiscordColor("FF0080"))
                                                    .WithDescription($"THIS USER IS TRYING TO HUG THEMSELVES!\nHOW LONELY CAN YOU BE!\n HAHAHA!")
                                                    .WithImageUrl(Rng.NextElement(LaughImages))
                                                    );
                return;
            }

            // Send an embed with a random hug image
            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription($"{ctx.User.Mention} hugs {targetMember.Mention}!")
                                                   .WithImageUrl(Rng.NextElement(HugImages))
                                                   );
        }


        [Command("pat"), Aliases("headpat")]
        public async Task Pat(CommandContext ctx, [RemainingText] string userIdentifier)
        {
            // If there is no user identifier, send a unique message
            if (userIdentifier is null)
            {
                await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                       .WithColor(new DiscordColor("FF0080"))
                                                       .WithDescription($"here you go {ctx.User.Mention}!\n^_^")
                                                       .WithImageUrl(Rng.NextElement(PatImages)));
                return;
            }

            // Get the user by their identifier, alert the user if none found
            var targetMember = await GetMemberByIdentifier(ctx, userIdentifier);
            if (targetMember is null)
            {
                await ctx.Channel.SendMessageAsync("Unable to find the user");
                return;
            }

            // Send an embed with a random hug image
            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription($"{ctx.User.Mention} pats {targetMember.Mention}!")
                                                   .WithImageUrl(Rng.NextElement(PatImages))
                                                   );
        }


        [Command("kiss"), Aliases("smooch")]
        public async Task Kiss(CommandContext ctx, [RemainingText] string userIdentifier)
        {
            // If there is no user identifier, send a unique message
            if (userIdentifier is null)
            {
                await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                       .WithColor(new DiscordColor("FF0080"))
                                                       .WithDescription($"chuuu {ctx.User.Mention}")
                                                       .WithImageUrl(Rng.NextElement(KissImages)));
                return;
            }

            // Get the user by their identifier, alert the user if none found
            var targetMember = await GetMemberByIdentifier(ctx, userIdentifier);
            if (targetMember is null)
            {
                await ctx.Channel.SendMessageAsync("Unable to find the user");
                return;
            }

            // Send an embed with a random hug image
            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription($"{ctx.User.Mention} kisses {targetMember.Mention}!")
                                                   .WithImageUrl(Rng.NextElement(KissImages))
                                                   );
        }


        [Command("smack"), Aliases("hit", "slap")]
        public async Task Smack(CommandContext ctx, [RemainingText] string userIdentifier)
        {
            // If there is no user identifier, send a unique message
            if (userIdentifier is null)
            {
                await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                       .WithColor(new DiscordColor("FF0080"))
                                                       .WithDescription($"{ctx.User.Mention} is a filthy masochist who likes it when discord bots hit him!")
                                                       .WithImageUrl(Rng.NextElement(SmackImages)));
                return;
            }

            // Get the user by their identifier, alert the user if none found
            var targetMember = await GetMemberByIdentifier(ctx, userIdentifier);
            if (targetMember is null)
            {
                await ctx.Channel.SendMessageAsync("Unable to find the user");
                return;
            }

            // Send an embed with a random hug image
            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription($"{ctx.User.Mention} smacks {targetMember.Mention}!")
                                                   .WithImageUrl(Rng.NextElement(SmackImages))
                                                   );
        }


        [Command("stare")]
        public async Task Stare(CommandContext ctx, [RemainingText] string userIdentifier)
        {
            // If there is no user identifier, send a unique message
            if (userIdentifier is null)
            {
                await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                       .WithColor(new DiscordColor("FF0080"))
                                                       .WithDescription($"i am judging you silently, {ctx.User.Mention}...")
                                                       .WithImageUrl(Rng.NextElement(StareImages)));
                return;
            }

            // Get the user by their identifier, alert the user if none found
            var targetMember = await GetMemberByIdentifier(ctx, userIdentifier);
            if (targetMember is null)
            {
                await ctx.Channel.SendMessageAsync("Unable to find the user");
                return;
            }

            // Send an embed with a random hug image
            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription($"{ctx.User.Mention} stares at {targetMember.Mention}!")
                                                   .WithImageUrl(Rng.NextElement(StareImages))
                                                   );
        }


        [Command("question"), Aliases("confused")]
        [Description("Confused reaction")]
        public async Task Question(CommandContext ctx, [RemainingText] [Optional] string userIdentifier)
        {
            // If there is no user identifier, send a unique message
            if (userIdentifier is null)
            {
                await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                       .WithColor(new DiscordColor("FF0080"))
                                                       .WithDescription($"{Rng.NextElement(new[] { "wha?", "huh?", "eh?" })}")
                                                       .WithImageUrl(Rng.NextElement(QuestionImages)));
                return;
            }

            // Get the user by their identifier, alert the user if none found
            var targetMember = await GetMemberByIdentifier(ctx, userIdentifier);
            if (targetMember is null)
            {
                await ctx.Channel.SendMessageAsync("Unable to find the user");
                return;
            }

            // Send an embed with a random hug image
            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription($"{ctx.User.Mention} pats {targetMember.Mention}!")
                                                   .WithImageUrl(Rng.NextElement(QuestionImages))
                                                   );
        }


        [Command("boobies"), Aliases("boobs", "titties", "tits", "funbags")]
        public async Task Boobies(CommandContext ctx)
        {
            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription("Boobies!")
                                                   .WithImageUrl(Rng.NextElement(BoobiesImages)));
        }
    }
}
