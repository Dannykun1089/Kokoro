﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Kokoro.Extensions;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using static Kokoro.Extensions.DSharpPlus;

namespace Kokoro.Commands
{
    public class General : BaseCommandModule
    {
        private string InviteLink { get; }
        private string RepositoryUrl { get; }
        private string ProjectPage { get; }
        private Random Rng { get; }


        public General()
        {
            var jsonData = JObject.Parse(File.ReadAllText("./Static/Data/Misc.json"));
            InviteLink = jsonData.Value<string>("invite_link");
            RepositoryUrl = jsonData.Value<string>("repository_url");
            ProjectPage = jsonData.Value<string>("project_page");

            Rng = new Random((int)DateTimeOffset.UtcNow.ToUnixTimeSeconds());
        }


        [Command("say"), Aliases("echo")]
        public async Task Say(CommandContext ctx, [RemainingText] string message)
        {
            await ctx.Channel.SendMessageAsync(message);
        }


        // Get user by mention, nickname, username or if no argument is provided, the author's avatar
        [Command("avatar"), Aliases("profile", "avi", "pfp")]
        public async Task Avatar(CommandContext ctx, [RemainingText] string userIdentifier)
        {
            // Send back the author's avatar
            if (userIdentifier is null)
            {
                await new DiscordMessageBuilder()
                          .WithEmbed(new DiscordEmbedBuilder
                          {
                              Title = $"Avatar of {ctx.User.Username}#{ctx.User.Discriminator}",
                              Color = new DiscordColor("FF0080"),
                              ImageUrl = ctx.User.AvatarUrl
                          })
                          .AddComponents(new DiscordComponent[]
                          {
                              new DiscordLinkButtonComponent(ctx.User.AvatarUrl, "Url")
                          }).SendAsync(ctx.Channel);
                return;
            }

            // Try to get member by identifier, and notify the user if we can't
            var member = await GetMemberByIdentifier(ctx, userIdentifier);
            if (member is null)
            {
                await ctx.Channel.SendMessageAsync("Unable to find user");
                return;
            }

            // Send the avatar of the requested user in an embed
            await new DiscordMessageBuilder()
                      .WithEmbed(new DiscordEmbedBuilder
                      {
                          Title = $"Avatar of {member.Username}#{member.Discriminator}",
                          Color = new DiscordColor("FF0080"),
                          ImageUrl = member.AvatarUrl
                      })
                      .AddComponents(new DiscordComponent[]
                      {
                          new DiscordLinkButtonComponent(member.AvatarUrl, "Url")
                      }).SendAsync(ctx.Channel);
        }


        [Command("roll")]
        public async Task Roll(CommandContext ctx, string rollExpression)
        {
            string[] rollExpressionArray = rollExpression.ToLower().Split("d");

            // Return if the roll expression is malformed
            if (rollExpressionArray.Length != 2)
            {
                await ctx.Channel.SendMessageAsync("Usage: roll [number of dice]d[sides of dice]\nExample: roll 2d6");
                return;
            }

            // Attempt to convert the roll expression into roll and side numbers
            uint diceToRoll, diceMax;
            uint.TryParse(rollExpressionArray[0], out diceToRoll);
            uint.TryParse(rollExpressionArray[1], out diceMax);
            if (diceToRoll * diceMax == 0 || diceToRoll > int.MaxValue || diceMax > int.MaxValue)
            {
                await ctx.Channel.SendMessageAsync("Invalid values");
                return;
            }

            // Simulate a roll with a random number between diceToRoll and diceMax * diceToRoll
            int total = Rng.Next((int)diceToRoll, (int)((diceToRoll * diceMax) + 1));

            await ctx.Channel.SendMessageAsync(total.ToString());
        }


        [Command("coin"), Aliases("coinflip")]
        public async Task Coin(CommandContext ctx)
        {
            await ctx.Channel.SendMessageAsync(Rng.NextElement(new[] { "Heads", "Tails" }));
        }


        [Command("base64"), Aliases("b64")]
        public async Task Base64(CommandContext ctx, [RemainingText] string encoded)
        {
            try
            {
                var decoded = Encoding.ASCII.GetString(Convert.FromBase64String(encoded));
                await ctx.Channel.SendMessageAsync(decoded);
            }
            catch
            {
                await ctx.Channel.SendMessageAsync("Invalid input");
            }
        }


        [Command("invite")]
        public async Task Invite(CommandContext ctx)
        {
            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithTitle("Bot invite link")
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription($"[Invite kokoro to other servers]({InviteLink})"));
        }


        [Command("project"), Aliases("info")]
        public async Task Project(CommandContext ctx)
        {
            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithTitle("Project information")
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription($"[Project Page]({ProjectPage})\n[Repository]({RepositoryUrl})"));
        }
    }
}
