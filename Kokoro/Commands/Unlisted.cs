﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using Kokoro.Extensions;
using Newtonsoft.Json.Linq;
using System;
using System.IO;
using System.Threading.Tasks;

namespace Kokoro.Commands
{
    public class Unlisted : BaseCommandModule
    {
        private string CatchLink { get; }
        private string[] RolfQuotes { get; }
        private string[] CoomerQuotes { get; }

        private Random Rng { get; }


        public Unlisted()
        {
            JObject miscJson = JObject.Parse(File.ReadAllText("./Static/Data/Misc.json"));
            JObject quotesJson = JObject.Parse(File.ReadAllText("./Static/Data/Quotes.json"));

            CatchLink = miscJson.GetValue("catch_link").ToObject<string>();
            RolfQuotes = quotesJson.GetValue("rolf").ToObject<string[]>();
            CoomerQuotes = quotesJson.GetValue("coomer").ToObject<string[]>();

            Rng = new Random((int)DateTimeOffset.Now.ToUnixTimeSeconds());
        }


        [Command("catch")]
        [Hidden]
        public async Task Catch(CommandContext ctx)
        {
            await ctx.Channel.SendMessageAsync(CatchLink);
        }


        [Command("rolf")]
        [Hidden]
        public async Task Rolf(CommandContext ctx)
        {
            await ctx.Channel.SendMessageAsync(Rng.NextElement(RolfQuotes));
        }


        [Command("coomer")]
        [Hidden]
        public async Task Coomer(CommandContext ctx)
        {
            await ctx.Channel.SendMessageAsync(Rng.NextElement(CoomerQuotes));
        }
    }
}
