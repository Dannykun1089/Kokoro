﻿using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Converters;
using DSharpPlus.CommandsNext.Entities;
using DSharpPlus.Entities;
using Serilog;
using System.Collections.Generic;
using System.Linq;

namespace Kokoro.Commands
{
    public class HelpFormatter : BaseHelpFormatter
    {
        private DiscordEmbedBuilder EmbedBuilder;


        public HelpFormatter(CommandContext ctx) : base(ctx)
        {
            EmbedBuilder = new DiscordEmbedBuilder()
                               .WithColor(new DiscordColor("FF0080"));
        }


        // For help with specific commands
        public override BaseHelpFormatter WithCommand(Command command)
        {
            // Assemble usage string
            var usageString = "";
            foreach (var overload in command.Overloads)
            {
                usageString += $"```{command.Name} ";
                foreach (var arg in overload.Arguments)
                {
                    if (arg.IsOptional)
                    {
                        usageString += $"({arg.Name})";
                    }
                    else
                    {
                        usageString += $"<{arg.Name}>";
                    }
                }
                usageString += "```";
            }


            EmbedBuilder.Title = command.Name;

            if (command.Aliases.Count == 0)
            { EmbedBuilder.AddField("Aliases:", "N/A"); }
            else
            { EmbedBuilder.AddField("Aliases:", string.Join(", ", command.Aliases)); }

            EmbedBuilder.AddField("Usage:", usageString);
            EmbedBuilder.AddField("Description:", command.Description ?? "No description");

            return this;
        }


        // A listing of all commands and modules
        public override BaseHelpFormatter WithSubcommands(IEnumerable<Command> commands)
        {
            // Get a list of all modules (minus the default help module)
            var modules = commands.Select(command => command.Module.ModuleType.Name).Distinct().ToList();
            modules.Remove("DefaultHelpModule");

            // Add the modules as keys to a listing dict
            var modulesListing = new Dictionary<string, List<string>>();
            foreach (var module in modules)
            {
                modulesListing.Add(module, new List<string>());
            }
            
            // Add commands to the listing under their own module
            foreach (var command in commands)
            {
                var moduleName = command.Module.ModuleType.Name;

                // Skip over the default help module
                if (moduleName == "DefaultHelpModule") { continue; }

                // Add the command
                modulesListing[moduleName].Add(command.Name);
            }

            // Construct the embed from the listing
            foreach (var module in modulesListing)
            {
                var commandList = $"```{string.Join("```\n```", module.Value)}```";
                EmbedBuilder.AddField(module.Key, commandList);
            }

            return this;
        }


        public override CommandHelpMessage Build()
        {
            return new CommandHelpMessage(embed: EmbedBuilder);
        }
    }
}
