﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Lavalink;
using DSharpPlus.Lavalink.EventArgs;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static Kokoro.Extensions.DSharpPlus;

namespace Kokoro.Commands
{
    // LavalinkTrack can't store command context data so this is a container for that
    public class KokoroTrack
    {
        public DiscordMember RequestingUser { get; }
        public DiscordChannel RequestingChannel { get; }
        public LavalinkTrack LLTrack { get; }


        public KokoroTrack(LavalinkTrack llTrack, CommandContext ctx)
        {
            LLTrack = llTrack;
            RequestingUser = ctx.Member;
            RequestingChannel = ctx.Channel;
        }
    }


    public class GuildAudioInfo
    {
        public List<KokoroTrack> TrackList { get; set; }
        public List<KokoroTrack> Queue { get { return TrackList.Skip(1).ToList(); } }
        public KokoroTrack? CurrentTrack { get { return TrackList.FirstOrDefault(); } }
        public KokoroTrack LastPlayedTrack { get; set; }


        public GuildAudioInfo()
        {
            TrackList = new List<KokoroTrack>();
        }
    }


    // TODO: DB Integration (custom playlists?)
    public class Audio : BaseCommandModule
    {
        public static readonly int tracksPerPage = 8;


        // Dictionary of all track queues the bot is running
        public static Dictionary<DiscordGuild, GuildAudioInfo> GuildsAudioInfo;


        public Audio()
        {
            GuildsAudioInfo = new Dictionary<DiscordGuild, GuildAudioInfo>();
        }


        // Gets the audio info for the given guild, creates it if it doesn't exist
        public GuildAudioInfo GetAudioInfo(DiscordGuild guild)
        {
            if (!GuildsAudioInfo.ContainsKey(guild))
            {
                GuildsAudioInfo.Add(guild, new GuildAudioInfo());
            }

            return GuildsAudioInfo[guild];
        }


        // Once the current track finishes, keep playing the next track in the queue until there is nothing left
        public async Task TrackPlaybackFinished(LavalinkGuildConnection conn, TrackFinishEventArgs eventArgs)
        {
            // Get this guild's audio info
            var audioInfo = GetAudioInfo(conn.Guild);

            // Set the finished track as the last played, remove the finished track and load the next track
            audioInfo.LastPlayedTrack = audioInfo.TrackList.FirstOrDefault();
            audioInfo.TrackList.RemoveAt(0);
            var nextTrack = audioInfo.TrackList.FirstOrDefault();

            // If there's a track to play, play it
            if (nextTrack != null)
            {
                await nextTrack.RequestingChannel.SendMessageAsync($"Now playing: {nextTrack.LLTrack.Title} by {nextTrack.LLTrack.Author}");
                await conn.PlayAsync(nextTrack.LLTrack);
            }
        }


        // Joins the voice chat of the user issuing a command
        public async Task<LavalinkGuildConnection> JoinVc(CommandContext ctx)
        {
            var lavalink = ctx.Client.GetLavalink();

            // If the bot is in another VC, disconnect and rejoin in the correct one
            var guildVcConnection = lavalink.GetGuildConnection(ctx.Guild);
            if (guildVcConnection != null)
            {
                // If the bot is already in the correct VC we can return
                if (guildVcConnection.Channel.Users.Contains(ctx.User))
                {
                    return guildVcConnection;
                }
                await guildVcConnection.DisconnectAsync();
            }

            // Attempt to get the voice channel the user is in
            var channels = await ctx.Guild.GetChannelsAsync();
            var voiceChannel = channels.Where(channel => channel.Type == ChannelType.Voice)
                                       .FirstOrDefault(voiceChannel => voiceChannel.Users.Contains(ctx.User));
            if (voiceChannel is null)
            {
                return null;
            }

            // Connect
            guildVcConnection = await Program.LavalinkConnection.ConnectAsync(voiceChannel);
            guildVcConnection.PlaybackFinished += TrackPlaybackFinished;

            return guildVcConnection;
        }


        // Generates a track selection page given a list of tracks and a page number
        private static DiscordMessageBuilder GenerateTrackPage(IEnumerable<LavalinkTrack> tracks, int pageNum)
        {
            var embed = new DiscordEmbedBuilder()
                            .WithTitle("Select a track")
                            .WithColor(new DiscordColor("FF0080"));

            var tracksPage = tracks.Skip(pageNum * tracksPerPage).Take(tracksPerPage);

            var i = 1;
            foreach (var track in tracksPage)
            {
                // Get track duration
                var trackDuration = track.Length.ToString("hh':'mm':'ss");
                if (trackDuration.StartsWith("00:"))
                { trackDuration = trackDuration.Remove(0, 3); }

                embed = embed.AddField($"{i}: {track.Title}", $"By {track.Author}\n```{trackDuration}```");
                i++;
            }

            return new DiscordMessageBuilder()
                       .WithEmbed(embed)
                       .AddComponents(new DiscordComponent[]
                       {
                           new DiscordButtonComponent(ButtonStyle.Primary,
                                                          "audiotrack_pagescroll_left",
                                                          "<="),
                           new DiscordButtonComponent(ButtonStyle.Primary,
                                                          "audiotrack_pagescroll_right",
                                                          "=>"),
                           new DiscordButtonComponent(ButtonStyle.Danger,
                                                          "audiotrack_pagescroll_cancel",
                                                          "X")
                       });
        }


        [Command("join"), Aliases("j")]
        public async Task Join(CommandContext ctx)
        {
            if (await JoinVc(ctx) is null)
            {
                await ctx.Channel.SendMessageAsync("Unable to locate the VC you're in");
            }
        }


        [Command("play"), Aliases("p")]
        public async Task Play(CommandContext ctx, [RemainingText] string song)
        {
            var audioInfo = GetAudioInfo(ctx.Guild);

            LavalinkGuildConnection guildVcConnection;

            // If no arguments are given we atempt to resume an enqueued song
            if (song is null)
            {
                if (audioInfo.TrackList.FirstOrDefault() is null)
                {
                    await ctx.Channel.SendMessageAsync("Please specify a song to play");
                    return;
                }

                guildVcConnection = ctx.Client.GetLavalink().GetGuildConnection(ctx.Guild);
                await guildVcConnection.ResumeAsync();

                return;
            }

            LavalinkTrack track = null;

            // Connect to voice channel
            guildVcConnection = await JoinVc(ctx);
            if (guildVcConnection is null)
            {
                await ctx.Channel.SendMessageAsync("Unable to locate the VC you're in");
                return;
            }

            // Discern if the song text is a link or search
            if (Regex.IsMatch(song, @"^http(|s):\/\/"))
            {
                // Get the track by Url
                var search = await guildVcConnection.GetTracksAsync(new Uri(song));
                track = search.Tracks.FirstOrDefault();

                // If we can't get a song from the link alert the user and return
                if (track is null)
                {
                    await ctx.Channel.SendMessageAsync($"Unable to load link <{song}>");
                    return;
                }
            }
            else
            {
                // Search with the song string
                var searchResults = await guildVcConnection.GetTracksAsync(song);

                var currentPage = 0;
                var pageMessage = await ctx.Channel.SendMessageAsync(GenerateTrackPage(searchResults.Tracks, currentPage));

                var trackSelected = false;
                while (!trackSelected)
                {
                    // Wait for a track to be selected or a request for a new page
                    var newPageInteractionTask = pageMessage.WaitForButtonAsync(ctx.User);
                    var trackSelectInteractionTask = ctx.Channel.GetNextMessageAsync(msg => msg.Author == ctx.User, TimeSpan.FromSeconds(61)); // Timespan override so we know which will timeout first

                    // Button is pressed
                    if (Task.WaitAny(newPageInteractionTask, trackSelectInteractionTask) == 0)
                    {
                        var buttonInteraction = newPageInteractionTask.Result;

                        // If we timed out, alert the user and return
                        if (buttonInteraction.TimedOut)
                        {
                            await pageMessage.DeleteAsync();
                            await ctx.Channel.SendMessageAsync("Track selection timed out");
                            return;
                        }

                        // Go to next or previous page
                        switch (buttonInteraction.Result.Id)
                        {
                            case "audiotrack_pagescroll_left":
                                if (currentPage != 0)
                                {
                                    currentPage--;
                                }
                                break;
                            case "audiotrack_pagescroll_right":
                                if ((currentPage + 1) * tracksPerPage < searchResults.Tracks.Count())
                                {
                                    currentPage++;
                                }
                                break;
                            case "audiotrack_pagescroll_cancel":
                                await buttonInteraction.Result.Message.DeleteAsync();
                                return;
                        }

                        // Edit the page message
                        await buttonInteraction.Result.Interaction.CreateResponseAsync(InteractionResponseType.UpdateMessage,
                                                                                       new DiscordInteractionResponseBuilder(GenerateTrackPage(searchResults.Tracks, currentPage)));
                    }

                    // Track is selected
                    else
                    {
                        // We have a track selected so we dont need to loop anymore
                        trackSelected = true;

                        var trackSelectInteraction = trackSelectInteractionTask.Result;

                        // Try to get a valid track number from the response message
                        int trackNum = 0;
                        if (!int.TryParse(trackSelectInteraction.Result.Content, out trackNum) || trackNum < 1 || trackNum > tracksPerPage)
                        {
                            await pageMessage.DeleteAsync();
                            await ctx.Channel.SendMessageAsync("Invalid track number");
                            return;
                        }

                        // Get the track
                        track = searchResults.Tracks.ElementAt((currentPage * tracksPerPage) + trackNum - 1);

                        // Delete the search page and user response
                        await pageMessage.DeleteAsync();
                        await trackSelectInteraction.Result.DeleteAsync();
                    }
                }
            }

            // Add the track to the queue
            audioInfo.TrackList.Add(new KokoroTrack(track, ctx));

            // If the only track in the list is the one we added, start playing it
            if (audioInfo.TrackList.Count == 1)
            {
                await ctx.Channel.SendMessageAsync($"Now playing: {track.Title} by {track.Author}");
                await guildVcConnection.PlayAsync(track);
            }
            // Else we enqueue it
            else
            {
                await ctx.Channel.SendMessageAsync($"Queued: {track.Title} by {track.Author}");
            }
        }


        [Command("pause"), Aliases("stop")]
        public async Task Pause(CommandContext ctx)
        {
            var lavalink = ctx.Client.GetLavalink();

            var guildVcConnection = lavalink.GetGuildConnection(ctx.Guild);

            if (guildVcConnection.CurrentState.CurrentTrack is null)
            {
                await ctx.Channel.SendMessageAsync("No track queued");
                return;
            }

            await guildVcConnection.PauseAsync();
            await ctx.Channel.SendMessageAsync("Current track paused");
        }


        [Command("nowplaying"), Aliases("np")]
        public async Task NowPlaying(CommandContext ctx)
        {
            var audioInfo = GetAudioInfo(ctx.Guild);
            var guildVcConnection = ctx.Client.GetLavalink().GetGuildConnection(ctx.Guild);

            if (guildVcConnection is null || guildVcConnection.CurrentState.CurrentTrack is null)
            {
                await ctx.Channel.SendMessageAsync("No track queued");
                return;
            }

            // Get the current track, its length and how much long its been playing
            // (Note: PlaybackPosition is inacurate, consider using your own time tracking system)
            var currentTrack = audioInfo.CurrentTrack;
            var playbackDuration = guildVcConnection.CurrentState.PlaybackPosition.ToString("hh':'mm':'ss");
            var trackDuration = currentTrack.LLTrack.Length.ToString("hh':'mm':'ss");

            // Remove redundant 00 for hours if the track is < 1hr long
            if (trackDuration.StartsWith("00:"))
            {
                playbackDuration = playbackDuration.Remove(0, 3);
                trackDuration = trackDuration.Remove(0, 3);
            }


            await ctx.Channel.SendMessageAsync(new DiscordEmbedBuilder()
                                                   .WithTitle(currentTrack.LLTrack.Title)
                                                   .WithColor(new DiscordColor("FF0080"))
                                                   .WithDescription($"Requested by {currentTrack.RequestingUser.Username}")
                                                   .AddField("Duration: ", $"```{playbackDuration}/{trackDuration}```"));
        }


        [Command("queue"), Aliases("q")]
        public async Task Queue(CommandContext ctx)
        {
            var audioInfo = GetAudioInfo(ctx.Guild);

            // No tracks are enqueued 
            if (audioInfo.Queue.Count == 0)
            {
                await ctx.Channel.SendMessageAsync("No tracks queued");
                return;
            }

            var embed = new DiscordEmbedBuilder()
                            .WithTitle("Queue")
                            .WithColor(new DiscordColor("FF0080"));


            // We only output the first 10 tracks in the queue to conserve message space (consider using page system?)
            foreach (var track in audioInfo.Queue.Take(10))
            {
                // Get track duration
                var trackDuration = track.LLTrack.Length.ToString("hh':'mm':'ss");
                if (trackDuration.StartsWith("00:"))
                { trackDuration = trackDuration.Remove(0, 3); }

                embed.AddField($"{track.LLTrack.Title}", $"Requested by {track.RequestingUser.Username}\n```{trackDuration}```");
            }

            // List the additional tracks in queue as a number
            if (audioInfo.Queue.Count > 10)
            {
                embed.WithFooter($"+{audioInfo.Queue.Count - 10} More Tracks");
            }

            await ctx.Channel.SendMessageAsync(embed);
        }


        [Command("skip"), Aliases("s")] // TODO: Integrate DJ permissions once db is coded
        public async Task Skip(CommandContext ctx)
        {
            // Check for DJ permissions
            if (IsDj(ctx.Member))
            {
                var guildVcConnection = ctx.Client.GetLavalink().GetGuildConnection(ctx.Guild);

                await guildVcConnection.SeekAsync(guildVcConnection.CurrentState.CurrentTrack.Length);
                await ctx.Channel.SendMessageAsync($"{ctx.Member.Username} skipped track {guildVcConnection.CurrentState.CurrentTrack.Title}");
            }
            else
            {
                // Note to self: Add vote skip for non-djs later
                await ctx.Channel.SendMessageAsync($"you need DJ permissions to skip tracks");
            }
        }


        [Command("seek"), Aliases("goto")]
        public async Task Seek(CommandContext ctx, string seekPosStr)
        {
            var guildVcConnection = ctx.Client.GetLavalink().GetGuildConnection(ctx.Guild);

            // Custom timespan parsing for a more user friendly experience
            var times = seekPosStr.Split(":");
            var seconds = 0;
            var minutes = 0;
            var hours = 0;
            if (times.Length >= 1)
            {
                int.TryParse(times[^1], out seconds);
            }
            if (times.Length >= 2)
            {
                int.TryParse(times[^2], out minutes);
            }
            if (times.Length >= 3)
            {
                int.TryParse(times[^3], out hours);
            }

            // Seek to the time specified (may have lag)
            var seekPos = new TimeSpan(hours, minutes, seconds);
            await guildVcConnection.SeekAsync(seekPos);
            await ctx.Channel.SendMessageAsync($"Skipping to {seekPos}...");
        }


        [Command("shuffle")]
        public async Task Shuffle(CommandContext ctx)
        {
            var audioInfo = GetAudioInfo(ctx.Guild);
            var rng = new Random();

            // Shuffle the queue
            var shuffledQueue = audioInfo.Queue.OrderBy(track => rng.Next()).ToList();

            // Replace the order in the track list
            for (int i = 1; i < audioInfo.TrackList.Count; i++)
            {
                audioInfo.TrackList[i] = shuffledQueue[i - 1];
            }
            await ctx.Channel.SendMessageAsync("Shuffled the queue");
        }


        [Command("replay"), Aliases("repeat")]
        [Description("Adds the last played track to the queue")]
        public async Task Replay(CommandContext ctx)
        {
            var guildVcConnection = ctx.Client.GetLavalink().GetGuildConnection(ctx.Guild);
            var audioInfo = GetAudioInfo(ctx.Guild);

            if (audioInfo.LastPlayedTrack is null)
            {
                await ctx.Channel.SendMessageAsync("There is no track to replay");
                return;
            }

            var lastPlayed = audioInfo.LastPlayedTrack.LLTrack;
            audioInfo.TrackList.Add(audioInfo.LastPlayedTrack);

            // If the only track in the list is the one we added, start playing it
            if (audioInfo.TrackList.Count == 1)
            {
                await ctx.Channel.SendMessageAsync($"Now playing: {lastPlayed.Title} by {lastPlayed.Author}");
                await guildVcConnection.PlayAsync(lastPlayed);
            }
            // Else we enqueue it
            else
            {
                await ctx.Channel.SendMessageAsync($"Queued: {lastPlayed.Title} by {lastPlayed.Author}");
            }
        }

        [Command("remove"), Aliases("rm")]
        [Description("Removes a specific track from the queue")]
        public async Task Remove(CommandContext ctx, int position)
        {
            if (position < 1 || position > 10)
            {
                await ctx.Channel.SendMessageAsync("Please select a valid track to remove");
                return;
            }

            var audioInfo = GetAudioInfo(ctx.Guild);

            var track = audioInfo.TrackList[position];
            audioInfo.TrackList.RemoveAt(position);

            await ctx.Channel.SendMessageAsync($"Removed track {track.LLTrack.Title}");
        }

        [Command("clear"), Aliases("clr")]
        [Description("Clears the queue")]
        public async Task Clear(CommandContext ctx)
        {
            var audioInfo = GetAudioInfo(ctx.Guild);

            audioInfo.TrackList.RemoveRange(1, audioInfo.TrackList.Count - 1);

            await ctx.Channel.SendMessageAsync("Cleared the queue");
        }


        [Command("dj")]
        [Description("Manage DJ permissions within this server")]
        public async Task DJ(CommandContext ctx, string operation, [RemainingText] string memberIdentifier)
        {
            // Check if member has admin permissions
            
            if (!ctx.Member.Permissions.HasPermission(Permissions.Administrator) && ctx.Member.Id != Program.OwnerDiscordId)
            {
                await ctx.Channel.SendMessageAsync("You need admin permissions to add or remove DJ permissions");
                return;
            }

            if (operation == "add")
            {
                // Get member
                var member = await GetMemberByIdentifier(ctx, memberIdentifier);

                // Open connection to the database and add the userId and GuildId
                var dbConn = new NpgsqlConnection(Program.DbConnectionString);
                dbConn.Open();

                // Search the database for an already existing record
                var cmd = new NpgsqlCommand("SELECT * FROM DJPerms WHERE UserId=$1 AND GuildId=$2", dbConn)
                {
                    Parameters =
                        {
                            new NpgsqlParameter() { Value = (long)member.Id },
                            new NpgsqlParameter() { Value = (long)ctx.Guild.Id }
                        }
                };
                var npgsqlAdapter = new NpgsqlDataAdapter(cmd);
                var dt = new DataTable();
                npgsqlAdapter.Fill(dt);

                // If there is no record, add it
                if (dt.Rows.Count == 0)
                {
                    cmd = new NpgsqlCommand("INSERT INTO DJPerms (UserId, GuildId) VALUES ($1, $2)", dbConn)
                    {
                        Parameters =
                            {
                                new NpgsqlParameter() { Value = (long)member.Id },
                                new NpgsqlParameter() { Value = (long)ctx.Guild.Id }
                            }
                    };
                    cmd.ExecuteNonQuery();
                    await ctx.Channel.SendMessageAsync($"Gave {member.Username} DJ permissions"); ;
                }
                else
                {
                    await ctx.Channel.SendMessageAsync("This user already has DJ permissions");
                }

                dbConn.Close();
                return;
            }

            if (operation == "remove" || operation == "rm")
            {
                // Get member
                var member = await GetMemberByIdentifier(ctx, memberIdentifier);

                // Open connection to the database and remove the entry for this user in this guild
                var dbConn = new NpgsqlConnection(Program.DbConnectionString);
                dbConn.Open();
                var cmd = new NpgsqlCommand("DELETE FROM DJPerms WHERE UserId=$1 AND GuildId=$2", dbConn)
                {
                    Parameters =
                            {
                                new NpgsqlParameter() { Value = (long)member.Id },
                                new NpgsqlParameter() { Value = (long)ctx.Guild.Id }
                            }
                };
                cmd.ExecuteNonQuery();
                await ctx.Channel.SendMessageAsync($"Removed DJ permissions for {member.Username}");
                dbConn.Close();
                return;
            }

            await ctx.Channel.SendMessageAsync("Please select a valid operation ['add' or 'remove']");
        }


        public bool IsDj(DiscordMember member)
        {
            // Open connection to the database and retrieve data about the user's permissions in this guild
            var dbConn = new NpgsqlConnection(Program.DbConnectionString);
            dbConn.Open();

            var cmd = new NpgsqlCommand("SELECT * FROM DJPerms WHERE UserId=$1 AND GuildId=$2", dbConn)
            {
                Parameters =
                        {
                            new NpgsqlParameter() { Value = (long)member.Id },
                            new NpgsqlParameter() { Value = (long)member.Guild.Id }
                        }
            };
            var npgsqlAdapter = new NpgsqlDataAdapter(cmd);
            var dt = new DataTable();
            npgsqlAdapter.Fill(dt);
            dbConn.Close();

            // If there is no record, there are no permissions
            if (dt.Rows.Count == 0)
            {
                return false;
            }
            return true;
        }
    }
}
