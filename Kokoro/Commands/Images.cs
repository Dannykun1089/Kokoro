﻿using BooruSharp.Booru;
using BooruSharp.Search;
using BooruSharp.Search.Post;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using ImageMagick;
using Kokoro.Extensions;
using Kokoro.Extensions.ImageMagick;
using Newtonsoft.Json.Linq;
using Serilog;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;


namespace Kokoro.Commands
{
    public class Images : BaseCommandModule
    {
        private static readonly int discordFileSizeLimit = 8000000;                  // The max size in bytes a file can be before discord rejects it
        private static readonly float mhrRelativeCaptionSize = .20f;                 // Size of mhr image caption relative to source image height
        private static readonly int mhrFontPointDenominator = 10;                    // We divide the source image width by this to get the font size
        private static readonly int mhrImageSearchTimeout = 10000;                   // The time taken in ms before we give up searching for the last message
        private static readonly int sunnyImageWidth = 1280;                          // Height of the sunny image
        private static readonly int sunnyImageHeight = 720;                          // Width of the sunny image
        private static readonly int sunnyCaptionWidth = 720;                         // Width of the sunny image's caption
        private static readonly string sunnyFontPath = "./Static/Fonts/Textile.ttf"; // Path to the ttf file used by sunny
        private static readonly int sunnyFontSize = 72;

        private string[] TagBlacklist { get; }

        private Gelbooru GbClient { get; }
        private HttpClient HttpClient { get; }


        public Images()
        {
            GbClient = new Gelbooru();
            HttpClient = new HttpClient();

            TagBlacklist = JObject.Parse(File.ReadAllText("./Static/Data/TagBlacklist.json")).GetValue("blacklist").ToObject<string[]>();
        }


        [Command("sunny")]
        public async Task Sunny(CommandContext ctx, [RemainingText] string text)
        {
            // Measure time taken to create image
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            // Create black-background image
            var outputImage = new MagickImage(MagickColors.Black, sunnyImageWidth, sunnyImageHeight);

            // Create transparent caption image with settings
            var caption = new MagickImage(MagickColors.Transparent, sunnyCaptionWidth, 1);
            caption.Settings.Font = sunnyFontPath;
            caption.Settings.FontPointsize = sunnyFontSize;
            caption.Settings.StrokeColor = MagickColors.White;
            caption.Settings.FillColor = MagickColors.White;

            // Seperate input text into lines
            var lines = MagickLine.GetLines($"\"{text}\"",
                                            sunnyFontPath,
                                            sunnyFontSize,
                                            caption.Width);

            // Resize caption to fit the lines
            caption.Extent(caption.Width, lines[0].Height * lines.Length);

            // Draw each line onto the caption horizontally centered
            int lineHeight = 0;
            foreach (var line in lines)
            {
                caption.Draw(new DrawableText((caption.Width - line.Width) / 2,
                                              lineHeight + line.TextHeight,
                                              line.Text));
                lineHeight += line.Height;
            }

            // Composite caption over black image
            outputImage.Composite(caption,
                                  (outputImage.Width - caption.Width) / 2,
                                  (outputImage.Height - caption.Height) / 2,
                                  CompositeOperator.Over);

            // Write output image data as PNG to a new memory stream
            MemoryStream imageMemstream = new MemoryStream();
            outputImage.Write(imageMemstream, MagickFormat.Png);
            imageMemstream.Seek(0, SeekOrigin.Begin);

            // Send the image and the time taken to create it
            stopwatch.Stop();
            await ctx.Channel.SendMessageAsync(new DiscordMessageBuilder()
                                                   .WithFile("Sunny.png", imageMemstream)
                                                   .WithContent($"Image generated in {stopwatch.Elapsed.Milliseconds} ms"));
        }


        [Command("myhonestreaction"), Aliases("honestreaction", "mhr", "hr")]
        public async Task MyHonestReaction(CommandContext ctx, string url)
        {
            // Try to download image
            HttpResponseMessage resp;
            try
            {
                resp = await HttpClient.GetAsync(url);
            }
            catch
            {
                await ctx.Channel.SendMessageAsync("Download failed");
                return;
            }

            // The request was not successful
            if (!resp.IsSuccessStatusCode)
            {
                await ctx.Channel.SendMessageAsync($"Download failed: {resp.StatusCode}");
                return;
            }


            // Copy response content into new memorystream
            var inputImageData = new MemoryStream();
            resp.Content.ReadAsStreamAsync().Result.CopyTo(inputImageData);
            inputImageData.Seek(0, SeekOrigin.Begin);


            // Can't resend image if it's too large
            if (inputImageData.Length > discordFileSizeLimit)
            {
                await ctx.Channel.SendMessageAsync("Input image is too large, please use a smaller image");
                return;
            }


            // Process image data
            var mhrImage = CreateMHRImage(inputImageData);
            mhrImage.Seek(0, SeekOrigin.Begin);


            // Can't send image if it's too large, processing may have added data
            if (mhrImage.Length > discordFileSizeLimit)
            {
                await ctx.Channel.SendMessageAsync("Processed image is too large, please use a smaller image");
                return;
            }


            // Send the image and delete the command message
            await ctx.Channel.SendMessageAsync(new DiscordMessageBuilder()
                                                   .WithFile("MyHonestReaction.png", mhrImage));
            await ctx.Message.DeleteAsync();
        }


        [Command("myhonestreaction")]
        public async Task MyHonestReaction(CommandContext ctx)
        {
            MemoryStream inputImageData = null;

            // Try and get image from command message
            var imageAttatchment = ctx.Message.Attachments.LastOrDefault(attachment =>
                                                                         attachment.MediaType.StartsWith("image"));
            if (imageAttatchment != null)
            {
                inputImageData = new MemoryStream();
                HttpClient.GetStreamAsync(imageAttatchment.Url).Result.CopyTo(inputImageData);
                inputImageData.Seek(0, SeekOrigin.Begin);
            }


            // Try and get last image sent by the user
            if (inputImageData is null)
            {
                // Time the search
                var stopwatch = new Stopwatch();
                stopwatch.Start();

                // Get list of last 100 messages sent
                var messages = await ctx.Channel.GetMessagesAsync();
                while (messages.Count != 0)
                {
                    // Stop search if it takes longer than expected
                    if (stopwatch.ElapsedMilliseconds > mhrImageSearchTimeout)
                    {
                        await ctx.Channel.SendMessageAsync("Search for last message took too long");
                        return;
                    }

                    // Search messages for one sent by the command issuer with an image attachment
                    var lastMessage = messages.FirstOrDefault(msg =>
                                                              msg.Author.Id == ctx.Message.Author.Id &&
                                                              msg.Attachments.Where(
                                                                  attachment => attachment.MediaType.StartsWith("image"))
                                                                  .Count() != 0);

                    // If message found, get data of last image attatchment 
                    if (lastMessage != null)
                    {
                        // Get the last image attatchment from the message
                        var lastImage = lastMessage.Attachments.Last(attachment =>
                                                                     attachment.MediaType.StartsWith("image"));

                        // Get image data
                        inputImageData = new MemoryStream();
                        HttpClient.GetStreamAsync(lastImage.Url).Result.CopyTo(inputImageData);
                        inputImageData.Seek(0, SeekOrigin.Begin);
                        break;
                    }

                    // Try to get the next 100 messages
                    messages = await ctx.Channel.GetMessagesBeforeAsync(messages.Last().Id);
                }

                stopwatch.Stop();
            }


            // Can't resend image if it's too large
            if (inputImageData.Length > discordFileSizeLimit)
            {
                await ctx.Channel.SendMessageAsync("Input image is too large");
                return;
            }


            // Process image data
            var mhrImage = CreateMHRImage(inputImageData);
            mhrImage.Seek(0, SeekOrigin.Begin);


            // Can't send image if it's too large
            if (mhrImage.Length > discordFileSizeLimit)
            {
                await ctx.Channel.SendMessageAsync("Processed image is too big, please use a smaller image");
                return;
            }

            // Send the image and delete the command message
            await ctx.Channel.SendMessageAsync(new DiscordMessageBuilder()
                                                   .WithFile("MyHonestReaction.png", mhrImage));
            await ctx.Message.DeleteAsync();
        }


        // Processes image data for the MHR command
        public MemoryStream CreateMHRImage(Stream inputImageData)
        {
            // Create magick image from input data
            MagickImage inputImage = new MagickImage(inputImageData);

            // Create transparent caption image for text
            MagickImage caption = new MagickImage(MagickColors.White, inputImage.Width, 1);
            caption.Settings.Font = "Comic Sans";
            caption.Settings.FontPointsize = inputImage.Width / mhrFontPointDenominator;

            // Create lines
            var lines = MagickLine.GetLines("My honest reaction to that information",
                                            caption.Settings.Font,
                                            (int)caption.Settings.FontPointsize,
                                            inputImage.Width);

            // Extend caption bounds to fit lines
            caption.Extent(caption.Width, lines[0].Height * lines.Length);

            // Draw each line onto the caption
            int lineHeight = 0;
            foreach (MagickLine line in lines)
            {
                caption.Draw(new DrawableText((caption.Width - line.Width) / 2,
                                              lineHeight + line.TextHeight,
                                              line.Text));
                lineHeight += line.Height;
            }

            // Compose output image from input and caption
            MagickImage finalImage = new MagickImage();
            if ((int)(inputImage.Height * mhrRelativeCaptionSize) < caption.Height)
            {
                finalImage = new MagickImage(MagickColors.Transparent, inputImage.Width, inputImage.Height + caption.Height);
                finalImage.Composite(inputImage, 0, finalImage.Height - inputImage.Height, CompositeOperator.Over);
                finalImage.Composite(caption, 0, 0, CompositeOperator.Over);
            }
            else
            {
                finalImage = new MagickImage(MagickColors.White, inputImage.Width, (int)(inputImage.Height * (1 + mhrRelativeCaptionSize)));
                finalImage.Composite(inputImage, 0, finalImage.Height - inputImage.Height, CompositeOperator.Over);
                finalImage.Composite(caption,
                                     0,
                                     ((int)(inputImage.Height * mhrRelativeCaptionSize) - caption.Height) / 2,
                                     CompositeOperator.Over);
            }


            // Create memorystream and write output image data to it
            MemoryStream outputStream = new MemoryStream();
            finalImage.Write(outputStream, MagickFormat.Png);

            return outputStream;
        }


        [Command("gelbooru"), Aliases("gb")]
        public async Task Gelbooru(CommandContext ctx, params string[] tags)
        {
            // Tags are in blacklist
            if (tags.Intersect(TagBlacklist).Count() != 0)
            {
                await ctx.Channel.SendMessageAsync($"Invalid tags: {string.Join(", ", tags)}");
                return;
            }

            bool resultAquired = false;
            SearchResult result = new SearchResult();
            while (!resultAquired)
            {
                try
                {
                    result = await GbClient.GetRandomPostAsync(tags);
                    if (result.Tags.Intersect(TagBlacklist).Count() == 0)
                    {
                        resultAquired = true;
                    }
                }
                // Tags do not exist
                catch (InvalidTags)
                {
                    await ctx.Channel.SendMessageAsync($"Invalid tags: {string.Join(", ", tags)}");
                    return;
                }
                catch (Exception e)
                {
                    Log.Error(e.Message);
                    await ctx.Channel.SendMessageAsync("Unknown error occured");
                    return;
                }
            }


            await ctx.Channel.SendMessageAsync(new DiscordMessageBuilder()
                                                   .WithEmbed(new DiscordEmbedBuilder()
                                                                  .WithColor(new DiscordColor("FF0080"))
                                                                  .WithImageUrl(result.FileUrl)
                                                                  .AddField("Tags:", string.Join(", ", tags))
                                                                  .AddField("Taken from:", "[gelbooru.com](https://gelbooru.com)")
                                                                  .Build())
                                                   .AddComponents(new DiscordComponent[] {
                                                                      new DiscordLinkButtonComponent(result.PostUrl.AbsoluteUri, "View post"),
                                                                      new DiscordLinkButtonComponent(result.FileUrl.AbsoluteUri, "View full")}));

            
        }
    }
}
