﻿using System;
using System.Collections.Generic;

namespace Kokoro.Extensions
{
    public static class Misc
    {
        public static T NextElement<T>(this Random rng, IList<T> list)
        {
            return list[rng.Next(list.Count)];
        }
    }
}
