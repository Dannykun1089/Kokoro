﻿using ImageMagick;
using System.Collections.Generic;
using System.Linq;

namespace Kokoro.Extensions.ImageMagick
{
    // Represents a line on a bitmap with dimensions
    public class MagickLine
    {
        public string Text { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int TextHeight { get; set; }


        public MagickLine(string text, int width, int height, int textHeight)
        {
            Text = text;
            Width = width;
            Height = height;
            TextHeight = textHeight;
        }


        public MagickLine Clone()
        {
            return new MagickLine(Text, Width, Height, TextHeight);
        }


        public static MagickLine[] GetLines(string text, string font, long fontPointSize, int width)
        {
            List<MagickLine> lines = new List<MagickLine>();

            // Create empty MagickImage to measure font dimensions
            using (var fontImg = new MagickImage())
            {
                // Set font and font size
                fontImg.Settings.Font = font;
                fontImg.Settings.FontPointsize = fontPointSize;

                // Get font height and space char width
                int spaceWidth = (int)fontImg.FontTypeMetrics(" ").TextWidth;
                int lineHeight = (int)fontImg.FontTypeMetrics(" ").TextHeight;

                MagickLine currentLine = new MagickLine("", 0, lineHeight, 0);

                // Itterate over the words in the caption text
                string[] words = text.Split(" ").Where(word => word != "").ToArray();
                for (int i = 0; i < words.Length; i++)
                {
                    int wordWidth = (int)fontImg.FontTypeMetrics(words[i]).TextWidth;

                    // If adding this word wouldn't exceed max width
                    if (currentLine.Width + wordWidth + spaceWidth < width)
                    {
                        // Add the word to the line and increment the line length
                        currentLine.Text += words[i] + " ";
                        currentLine.Width += wordWidth + spaceWidth;
                    }
                    else
                    {
                        // Trim the current line and add to the list
                        currentLine.Text = currentLine.Text.TrimEnd();
                        currentLine.Width -= spaceWidth;
                        currentLine.TextHeight = (int)fontImg.FontTypeMetrics(currentLine.Text).Ascent;
                        lines.Add(currentLine.Clone());

                        // Load the current word into a new current line
                        currentLine.Text = words[i] + " ";
                        currentLine.Width = wordWidth + spaceWidth;
                    }

                    // If this is the last word
                    if (i + 1 == words.Length)
                    {
                        // Trim the current line and add to the list
                        currentLine.Text = currentLine.Text.TrimEnd();
                        currentLine.Width -= spaceWidth;
                        currentLine.TextHeight = (int)fontImg.FontTypeMetrics(currentLine.Text).Ascent;
                        lines.Add(currentLine.Clone());
                    }
                }
            }
            return lines.ToArray();
        }
    }
}
