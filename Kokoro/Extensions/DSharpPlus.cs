﻿using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace Kokoro.Extensions
{
    public static class DSharpPlus
    {
        // Tries to get a discord user by their id, mention, username or nickname
        public static async Task<DiscordMember?> GetMemberByIdentifier(CommandContext ctx, string identifier)
        {
            DiscordMember member = null;

            // Attempt to parse a user ID
            ulong userId = 0;
            ulong.TryParse(identifier, out userId);

            if (userId == 0 && identifier.Length > 3)
            {
                ulong.TryParse(identifier[2..^1], out userId);
            }


            // Attempt to get member by Id
            var memberList = await ctx.Guild.GetAllMembersAsync();
            if (memberList.Select(member => member.Id).Contains(userId))
            {
                member = await ctx.Guild.GetMemberAsync(userId);
            }

            identifier = identifier.ToLower();

            // Attempt to get member by Username
            member ??= memberList.FirstOrDefault(member => member.Username.ToLower().Contains(identifier));

            // Attempt to get member by Nickname
            member ??= memberList.Where(member => member.Nickname != null) 
                                  .FirstOrDefault(member => member.Nickname.ToLower().Contains(identifier));

            return member;
        }
    }
}
