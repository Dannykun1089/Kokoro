﻿using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.EventArgs;
using DSharpPlus.Interactivity;
using DSharpPlus.Interactivity.Enums;
using DSharpPlus.Interactivity.Extensions;
using DSharpPlus.Lavalink;
using DSharpPlus.Net;
using Microsoft.Extensions.Logging;
using Npgsql;
using Serilog;
using System;
using System.Threading.Tasks;

namespace Kokoro
{
    public static class Program
    {
        // A list of environment variables the program is dependant on
        private static readonly string[] requiredEnvironmentVariables = { "BOT_TOKEN",
                                                                          "POSTGRES_HOST",
                                                                          "POSTGRES_USERNAME",
                                                                          "POSTGRES_PASSWORD",
                                                                          "LAVALINK_HOST",
                                                                          "LAVALINK_PORT",
                                                                          "LAVALINK_PASSWORD",
                                                                          "OWNER_DISCORD_ID" };


        public static DiscordClient BotClient;
        public static LavalinkNodeConnection LavalinkConnection;
        public static string DbConnectionString;
        public static ulong OwnerDiscordId;  


        public static void Main()
        {
            AsyncMain().GetAwaiter().GetResult();
        }


        public static async Task AsyncMain()
        {
            // Create Logger
            Log.Logger = new LoggerConfiguration()
               .MinimumLevel.Information()
               .WriteTo.Console(outputTemplate: "[{Timestamp:dd/MM/yyyy}] [{Timestamp:hh:mm:ss zzz}] [{Level:u4}] {Message:lj}{NewLine}{Exception}")
               .CreateLogger();

            // Check to see if environment variables are present
            bool envVarsValid = true;
            foreach (var envVar in requiredEnvironmentVariables)
            {
                if (Environment.GetEnvironmentVariable(envVar) is null)
                {
                    Log.Fatal("Environment variable {envVar} is not present", envVar);
                    envVarsValid = false;
                }
            }

            // Validate environment variables
            if (!ulong.TryParse(Environment.GetEnvironmentVariable("OWNER_DISCORD_ID"), out OwnerDiscordId))
            {
                Log.Fatal("Environment variable {envVar} is not a valid number", OwnerDiscordId);
                envVarsValid = false;
            }

            if (!envVarsValid) { Environment.Exit(1); }

            // Establish connection to Postgresql database
            var pgHost = Environment.GetEnvironmentVariable("POSTGRES_HOST");
            var pgUsername = Environment.GetEnvironmentVariable("POSTGRES_USERNAME");
            var pgPassword = Environment.GetEnvironmentVariable("POSTGRES_PASSWORD");
            NpgsqlConnection DbConnection = null;
            try
            {
                DbConnectionString = $"Host={pgHost};Username={pgUsername};Password={pgPassword};Database=Kokoro";
                DbConnection = new NpgsqlConnection(DbConnectionString);
                DbConnection.Open();
            }
            catch (Exception e)
            {
                Log.Fatal("Unable to connect to {host}\n{exception}", pgHost, e);
                Environment.Exit(1);
            }

            var cmd = new NpgsqlCommand(@"CREATE TABLE IF NOT EXISTS DJPerms (
                                          UserId BIGINT NOT NULL,
                                          GuildId BIGINT NOT NULL
                                          )", DbConnection);
            cmd.ExecuteNonQuery();
            DbConnection.Close();

            // Create discord client
            BotClient = new DiscordClient(new DiscordConfiguration
            {
                Token = Environment.GetEnvironmentVariable("BOT_TOKEN"),
                TokenType = TokenType.Bot,
                Intents = DiscordIntents.AllUnprivileged | DiscordIntents.GuildMembers,
                LoggerFactory = new LoggerFactory().AddSerilog(Log.Logger)
            });

            // Register command modules
            CommandsNextExtension commands = BotClient.UseCommandsNext(new CommandsNextConfiguration
            {
                StringPrefixes = new[] { "k ", "k> " },
                EnableDms = true
            });

            commands.RegisterCommands<Commands.General>();
            commands.RegisterCommands<Commands.Emotes>();
            commands.RegisterCommands<Commands.Images>();
            commands.RegisterCommands<Commands.Audio>();
            commands.RegisterCommands<Commands.Unlisted>();

            // Set custom help message generator
            commands.SetHelpFormatter<Commands.HelpFormatter>();


            // Enable interactivity
            BotClient.UseInteractivity(new InteractivityConfiguration()
            {
                PollBehaviour = PollBehaviour.KeepEmojis,
                Timeout = TimeSpan.FromSeconds(60),
            });


            // Establish connection to lavalink server
            var LLEndpoint = new ConnectionEndpoint()
            {
                Hostname = Environment.GetEnvironmentVariable("LAVALINK_HOST"),
                Port = int.Parse(Environment.GetEnvironmentVariable("LAVALINK_PORT"))
            };

            var LLConfig = new LavalinkConfiguration()
            {
                Password = Environment.GetEnvironmentVariable("LAVALINK_PASSWORD"),
                RestEndpoint = LLEndpoint,
                SocketEndpoint = LLEndpoint
            };

            // Add event handlers
            BotClient.Ready += BotClient_Ready;


            // Connect to discord and run the bot
            await BotClient.ConnectAsync();

            // Connect to the lavalink server
            LavalinkConnection = await BotClient.UseLavalink().ConnectAsync(LLConfig);

            // Wait forever
            await Task.Delay(-1);
        }


        // Logs when the bot is ready
        private static Task BotClient_Ready(DiscordClient sender, ReadyEventArgs e)
        {
            Log.Information("Bot ready");
            return Task.CompletedTask;
        }
    }
}
